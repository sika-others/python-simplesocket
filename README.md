python-simplesocket
===================

Simple socket server for Python

### Authors
*  Ondrej Sika, <http://ondrejsika.com>, dev@ondrejsika.com

### Source
* Python Package Index: <http://pypi.python.org/pypi/simplesocket>
* GitHub: <https://github.com/sikaondrej/python-simplesocket>


Documentation
-------------

### Instalation
Instalation is very simple via pip.

    pip install simplesocket


### Example echo server

    from simplesocket import Server

    def handler(req):
        return req
    
    server = Server(("localhost", 9999), handler)

    server.serve_forever()


### Example client

    from simplesocket import client_request

    request = "hello"
    response = client_request(("localhost", 9999), request)
