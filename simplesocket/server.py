#!/usr/bin/python
# coding: utf8

# Simple server
# authors: Ondrej Sika, http://ondrejsika.com, dev@ondrejsika.com


import SocketServer

def Server(address, handler, request_size=16777216):
    class TCPHandler(SocketServer.BaseRequestHandler):
        def handle(self):
            self.data = self.request.recv(16*1024*1024).strip()
            self.request.sendall(handler(self.data))
    return SocketServer.TCPServer(address, TCPHandler)
