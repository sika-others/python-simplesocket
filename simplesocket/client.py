#!/usr/bin/python
# coding: utf8

# Simple server
# authors: Ondrej Sika, http://ondrejsika.com, dev@ondrejsika.com


import socket

def client_request(address, request, response_size=16777216):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(address)
    sock.sendall(request)
    res = sock.recv(response_size)
    sock.close()
    return res
