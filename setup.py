#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "simplesocket",
    version = "1.0.0",
    url = 'http://ondrejsika.com/docs/python-simplesocket',
    download_url = 'https://github.com/sikaondrej/python-simplesocket',
    license = 'GNU LGPL v.3',
    description = "",
    author = 'Ondrej Sika',
    author_email = 'dev@ondrejsika.com',
    py_modules = [],
    packages = find_packages(),
    install_requires = [],
    include_package_data = True,
    zip_safe = False,
)
